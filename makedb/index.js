/**
 * Created by user on 06/10/2017.
 */
'use strict'


module.exports = function (next) {
    if(process.env.NODE_ENV == 'development') return next();

    switch (process.env.DB_ADAPTER) {
        case("postgres"):
            return require("./dbs/pg").run(next);
        default:
            console.log("No DB Adapter defined. Using localDB...");
            return next();

    }
}




