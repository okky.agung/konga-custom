module.exports = {
  apps : [{
    name      : 'app',
    script    : './app.js',
    env_production : {
      NODE_ENV: 'production'
    }, env: {
      NODE_ENV: 'development'
    }
  }]
};
